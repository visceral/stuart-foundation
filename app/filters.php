<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );
    return template_path(locate_template(["views/{$comments_template}", $comments_template]) ?: $comments_template);
});


/**
 * Allow SVG Uploads
 */
function theme_allow_svg_uploads($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__.'\\theme_allow_svg_uploads');
// End Allow SVG Uploads

/**
 * Custom Font Formats
 */
function theme_tiny_mce_formats_button($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', __NAMESPACE__ . '\\theme_tiny_mce_formats_button');

// Callback function to filter the MCE settings
function theme_tiny_mce_custom_font_formats($init_array) {
    // Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
        array(
            'title' => 'Split Divider',
            'selector' => '*',
            'classes' => 'split-divider'
        ),
        array(
            'title' => 'Button',
            'selector' => 'a',
            'classes' => 'btn button'
        ),
        array(
            'title' => 'Number Ticker',
            'inline' => 'span',
            'classes' => 'ticker'
        )
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}
add_filter('tiny_mce_before_init', __NAMESPACE__ . '\\theme_tiny_mce_custom_font_formats'); // Attach callback to 'tiny_mce_before_init'
// End Custom Font Formats

/**
 * Query for all post statuses so attachments are returned
 */
function visceral_modify_link_query_args($query) {
    // Attachment post types have status of inherit. This allows them to be included.
    $query['post_status'] = array('publish', 'inherit');
    return $query;
}
add_filter('wp_link_query_args', __NAMESPACE__ . '\\visceral_modify_link_query_args');

/**
 * Link to media file URL instead of attachment page
 */
function visceral_modify_link_query_results($results, $query) {
    foreach ($results as &$result) {
        if ('Media' === $result['info']) {
            $result['permalink'] = wp_get_attachment_url($result['ID']);
        }
    }
    return $results;
}

add_filter('wp_link_query', __NAMESPACE__ . '\\visceral_modify_link_query_results');

/**
 * Filter Search by Post Type
 */
function filter_search_by_post_type($post_types) {
    if (!empty($_GET['filter'])) {
        $post_types = array( sanitize_text_field($_GET['filter']) );
    }
    return $post_types;
}
add_filter('searchwp_indexed_post_types', __NAMESPACE__ . '\\filter_search_by_post_type');

/**
 * LOWER YOAST META BOX
 */
function yoast_dont_boast($html) {
    return 'low';
}
add_filter('wpseo_metabox_prio', __NAMESPACE__ . '\\yoast_dont_boast');
// END LOWER YOAST META BOX

<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);

    // Main Sections
    $wp_customize->add_section(
        'theme_general',
        array(
            'title' => __('General Options', 'visceral'),
            'priority' => 90,
        )
    );
    $wp_customize->add_section(
        'theme_social_media',
        array(
            'title' => __('Social Media', 'visceral'),
            'priority' => 90,
        )
    );

    $wp_customize->add_section(
        'theme_login',
        array(
            'title' => __('Login', 'visceral'),
            'priority' => 90,
        )
    );

    $wp_customize->add_setting(
        'facebook_url',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'facebook_url',
        array(
            'label' => __('Facebook URL', 'visceral'),
            'section' => 'theme_social_media',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'twitter_url',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'twitter_url',
        array(
            'label' => __('Twitter URL', 'visceral'),
            'section' => 'theme_social_media',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'linkedin_url',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'linkedin_url',
        array(
            'label' => __('LinkedIn URL', 'visceral'),
            'section' => 'theme_social_media',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'youtube_url',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'youtube_url',
        array(
            'label' => __('YouTube URL', 'visceral'),
            'section' => 'theme_social_media',
            'type' => 'text',
        )
    );
    
    $wp_customize->add_setting(
        'copyright_textbox',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'copyright_textbox',
        array(
            'label' => __('Copyright text', 'visceral'),
            'section' => 'theme_social_media',
            'type' => 'text',
        )
    );
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

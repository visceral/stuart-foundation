<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    if (remove_action('wp_head', 'wp_enqueue_scripts', 1)) {
        wp_enqueue_scripts();
    }

    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                        "{$template}.blade.php",
                        "{$template}.php",
                    ];
                });
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}

/**
 * Visceral Query
 *
 * @return  The markup for a list of posts based on query parameters and template files.
 */
function visceral_query_shortcode($atts) {
    extract(shortcode_atts(array(
        'container_class' => '',
        'template' => 'templates/list-item',
        'show_pagination' => 0,
        'no_posts' => '',
        'post_type' => 'post',
        'posts_per_page' => 6,
        'offset' => 0,
        'order' => 'DESC',
        'orderby' => 'post_date',
        'post__not_in' => '',
        'taxonomy' => '',
        'terms' => ''
    ), $atts));

    $markup = '';

    // Return either a single post type or an array of post types depending on how many were passed in.
    // Note: We need to do this because the Intuitive Custom Post Type plugin won't work with an array of a single post type.
    $post_type = (strpos($post_type, ',') > 0) ? explode(', ', $post_type) : $post_type;

    $args = array(
        'post_type' => $post_type,
        'posts_per_page' => $posts_per_page,
        'offset' => $offset,
        'order' => $order,
        'orderby' => $orderby,
        'post__not_in' => explode(', ', $post__not_in),
    );
    if ($taxonomy && $terms) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => explode(', ', $terms),
            ),
        );
    }
    $query = new \WP_Query($args);

    if ($query->have_posts()) :
        $markup .= '<div class="' . $container_class . '">';
        while ($query->have_posts()) :
            $query->the_post();
            ob_start();
            get_template_part($template, get_post_type());
            $markup .= ob_get_contents();
            ob_end_clean();
        endwhile;
        $markup .= '</div>';
        wp_reset_postdata();

        // Pagination
        $total = $query->max_num_pages;
        // only bother with pagination if we have more than 1 page
        if ($show_pagination && $total > 1) :
            $big = 999999999; // need an unlikely integer
            $current_page = max(1, get_query_var('paged'));
            $pagination = paginate_links(array(
                'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format'    => '?paged=%#%',
                'current'   => $current_page,
                'total'     => $total,
                'type'      => 'plain',
                'prev_next' => true,
                'prev_text' => '<span class="icon-angle-left">' . __('Previous', 'visceral') . '</span>',
                'next_text' => '<span class="icon-angle-right">' . __('Next', 'visceral') . '</span>'
            ));

            $markup .= '<nav class="pagination text-center">';
                $markup .= $pagination;
            $markup .= '</nav>';
        endif;
    else :
        $markup = $no_posts;
    endif;

    return $markup;
}
add_shortcode('visceral_query', __NAMESPACE__ . '\\visceral_query_shortcode');

/* END SHORTCODES */

/* GENERAL PURPOSE FUNCTIONS */

/**
 * RELATED POSTS FUNCTION - Creates an array of related post objects
 *
 * @param int   $total_posts                 Total posts to display
 * @param mixed $post_type                   Either a string or array of post types to display
 * @param string $related_posts_field_name   Name of the manually selected related posts ACF field
 *
 * @return array                             Returns an of related posts
 */
function visceral_related_posts($posts_per_page = 3, $post_type = 'post', $related_posts_field_name = 'related_posts') {
    // Set up the an array to hold them
    $related_posts = array();
    // Get post ID's from related field
    $selected_posts_ids = get_post_meta(null, $related_posts_field_name, true);

    // Add each post object to our array
    // Subtract each post from total posts
    if ($selected_posts_ids) {
        $args = array(
            'include' => $selected_posts_ids,
        );
        $selected_posts = get_posts($args);
        foreach ($selected_posts as $post_object) {
            array_push($related_posts, $post_object);
            $posts_per_page --;
        }
    }

    // If we need more posts, let's get some from recent posts
    if ($posts_per_page) {
        // Don't repeat the posts that we already have
        $exclude_posts = $selected_posts_ids ? $selected_posts_ids : []; // Make sure it's an array
        array_push($exclude_posts, get_the_id());

        $args = array(
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page,
            'exclude' => $exclude_posts,
        );
        $even_more_posts = get_posts($args);
        foreach ($even_more_posts as $post_object) {
            array_push($related_posts, $post_object);
        }
    }

    return $related_posts;
}
// END RELATED POSTS FUNCTION


/**
* Takes a string and returns a truncated version. Also strips out shortcodes
*
* @param  string  $text   String to truncate
* @param  integer $length Character count limit for truncation
* @param  string  $append Appended to the end of the string if character count exceeds limit
* @return string          Truncated string
*/
function truncate_text($text = '', $length = 150, $append = '...') {
    $new_text = preg_replace(" ([.*?])", '', $text);
    $new_text = strip_shortcodes($new_text);
    $new_text = strip_tags($new_text);
    $new_text = substr($new_text, 0, $length);
    if (strlen($new_text) == $length) {
        $new_text = substr($new_text, 0, strripos($new_text, " "));
        $new_text = $new_text . $append;
    }

    return $new_text;
}

/**
* Returns an image with a class for fitting to a certain aspect ratio
*
* @param  integer $aspect_ratio_width   Aspect Ratio Width
* @param  integer $aspect_ratio_height   Aspect Ratio Height
* @param  string  $image_size   Size of featured image to return
* @return string  markup of image
*/
function get_aspect_ratio_image($aspect_ratio_width = 1, $aspect_ratio_height = 1, $image_size = 'medium') {
    $image = wp_get_attachment_image_src(get_post_thumbnail_id(), $image_size);
    $w     = $image[1];
    $h     = $image[2];
    $class = ( ($h / $w) > ($aspect_ratio_height / $aspect_ratio_width) ) ? 'portrait' : '';
    return get_the_post_thumbnail(get_the_id(), $image_size, array('class' => $class));
}

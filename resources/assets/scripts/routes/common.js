export default {
  init() {
    // JavaScript to be fired on all pages

    /** GLOBAL VARIABLES */
    // var body = document.querySelector('body');
    /** END GLOBAL VARIABLES */

    /** SKIP LINK NAVIGATION */
    $('#skip-to-content').click(function () {
      // strip the leading hash and declare
      // the content we're skipping to
      var skipTo = "#" + this.href.split('#')[1];
      // Setting 'tabindex' to -1 takes an element out of normal 
      // tab flow but allows it to be focused via javascript
      $(skipTo).attr('tabindex', -1).on('blur focusout', function () {
        // when focus leaves this element, 
        // remove the tabindex attribute
        $(this).removeAttr('tabindex');
      }).focus(); // focus on the content container
    });
    /** END SKIP LINK NAVIGATION */
    var test = 'test';
    console.log(test);
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};

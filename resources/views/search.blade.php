@extends('layouts.app')
<?php 
// Set up results numbers
global $wp_query;
$total = $wp_query->found_posts;
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$post_per_page = $wp_query->query_vars['posts_per_page'];
$offset = ( $paged - 1 ) * $post_per_page;
$begin = $offset + 1;
$end = ( $paged*$post_per_page < $total ) ? $paged * $post_per_page : $total;

// Save the original query
if ( isset( $_GET['s'] ) ) {
	$original_search = sanitize_text_field( $_GET['s'] );
}

// Create content for filter dropdown
$args = array(
	'public' => true,
);
$post_types = get_post_types( $args );
$filter_items = '';
if ( $post_types ) {
	$filter_items = '<option value="">' . __('Filter', 'visceral') . '</option>';
	foreach ( $post_types as $slug ) {
		if ( $slug != 'attachment' ) {
			$label = get_post_type_object( $slug )->labels->name;
			$filter_selected = (isset($_GET['filter']) &&  sanitize_text_field( $_GET['filter'] ) == $slug ) ? 'selected' : '';
			$filter_items .= '<option name="content_filter" value="' . $slug . '"' . $filter_selected . '>' . $label . '</option>';
		}
	}
} else {
	$filter_items .= '<option>' . __('No filters', 'visceral') . '</option>';
}?>
?>
@section('content')
  <div class="search-results-top">
    <form action="<?php echo site_url(); ?>" class="row md-reverse row-eq-height">
      <div class="column sm-100">
        <label><span class="screen-reader-text">{{ __('Search', 'visceral')}}</span>
          <input type="text" name="s" value="{{ $original_search }}" class="js-show">
        </label>
      </div>
      
      <div class="column sm-33">
        <label><span class="screen-reader-text">{{ __('Filter By:', 'visceral')}}</span>
          <select name="filter" onchange="this.form.submit()">{{!! $filter_items !!}}</select>
        </label>
      </div>
      <div class="js-show column sm-33">
        <input type="submit" value="{{ __('Submit', 'visceral')}}">
      </div>
      <div class="column sm-67">
        <h3 >{{$begin . '-' . $end . ' ' . __('of', 'visceral') . ' ' . $total}}</h3>
      </div>
    </form>
  </div>
  @if (!have_posts())
    <div class="alert alert-warning">
      {{  __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while(have_posts()) @php(the_post())
    @include('partials.content-search')
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection

<header class="header container-fluid">
    <div class="header__inner">
        <a class="header__brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
        <nav class="header__navigation">
            @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
            @endif
        </nav>
        <label class="header__nav-icon icon-menu no-print" for="nav-toggle" tabindex="0"></label>
		<a class="header__search-button icon-search" href="{{ site_url() }}/?s=" title="@php( _e('Search', 'visceral') )" aria-label="Search"><?php _e('Search', 'visceral'); ?></a>
    </div>
    <div class="header__search">
        <div>
            <form role="search" class="header__search-form searchform" method="get" action="{{ site_url() }}">
                <label><span class="screen-reader-text"><?php _e('Search', 'visceral'); ?></span>
                    <input type="text" placeholder="<?php _e('Type here and hit enter', 'visceral'); ?>..." name="s" autocomplete="off" spellcheck="false" autofocus>
                </label>
                <input type="submit" class="screen-reader-text" value="<?php _e('Submit', 'visceral'); ?>">
            </form>
            <i class="header__search-close icon-cancel" aria-label="Close"><?php _e('Close', 'visceral'); ?></i>
        </div>
    </div>
</header>

@php
/**
 * Masthead
 *
 * This file has a lot going on
 *
 * First we select the right hero image to load
 * Then we load in the small version as a placeholder, and the large version lazy loaded in.
 */

// Set up variable
$parent_id = '';
$image_id = '';
$full_size_image_url = '';
$placeholder_image_url = '';

// Start off with a check for custom header images early and get the ID
if ( function_exists('get_field') && get_field( 'custom_header' ) != '' ) {
	$custom_header = get_field('custom_header');
	$image_id = $custom_header['id'];
}

// Some pages/posts/archives may be part of a seciton and pull the header from a "parent"
elseif ( is_singular('post') ) {
	// For these, we get the "parent" ID and get the image later on.
	$parent_id = get_page_by_path( 'blog' )->ID;
}
elseif ( is_category() ) {
	$parent_id = get_page_by_path( 'about/whats-new' )->ID;
}

// There are templates some that just need custom images.
elseif ( is_search() || is_404() ) {
	// For static headers, we just load the same image for main and placeholder.
	$full_size_image_url = $placeholder_image_url = get_template_directory_uri() . '/dist/images/header-custom.jpg';
}

// Finally, if nothing else, we check for a featured image and get it's ID
elseif ( has_post_thumbnail() ) {
	$image_id = get_post_thumbnail_id();
}

// Remember the parent ID's we got above? Let's adopt their main image and get the ID
if ( $parent_id != '' ) {
	if ( function_exists('get_field') && get_field( 'custom_header', $parent_id ) != '' ) {
		$custom_header = get_field('custom_header', $parent_id);
		$image_id = $custom_header['id'];
	} elseif ( has_post_thumbnail( $parent_id ) ) {
		$image_id = get_post_thumbnail_id( $parent_id );
	}
}


// By now, we should have an image ID to use. Let's use it to get some URLs.
if ( $image_id != '' ) {
	$full_size_image = wp_get_attachment_image_src( $image_id,'full', true);
	$full_size_image_url = $full_size_image[0];

	$placeholder_image = wp_get_attachment_image_src( $image_id,'thumbnail', true);
	$placeholder_image_url = $placeholder_image[0];
}

/**
 * Markup from here on
 */
@endphp

<div class="masthead img-bg" data-image-src="{{ $full_size_image_url }}">
	<?php // The low quality placeholder
	if ( $placeholder_image_url != '' ) : ?>
		<span class="placeholder-overlay img-bg" style="background-image: url({{$placeholder_image_url}});"></span>
	<?php endif; ?>

	<div class="container text-white">
		<h1>{!! App::title() !!}</h1>
	</div>
</div>

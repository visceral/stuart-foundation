<footer class="content-info">
  <div class="container-fluid">
    @php(dynamic_sidebar('sidebar-footer'))
     @if (has_nav_menu('footer_menu'))
        {!! wp_nav_menu(['theme_location' => 'footer_menu', 'menu_class' => 'nav list-inline']) !!}
      @endif
      <p class="copyright">&copy; {{date('Y')}} {{ get_theme_mod( 'copyright_textbox', get_bloginfo('name') )}}</p>
  </div>
</footer>
